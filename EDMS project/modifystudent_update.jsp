<%-- 
    Document   : modifystudent_update
    Created on : 18 Jan, 2019, 3:51:59 PM
    Author     : USER
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="Home1page.css">
    </head>
    <body>
       <div id ="navHome">
            <ul>
               <li class="active"><a href="Home1page.jsp">Home</a></li>
               
               <li class="dropdown"><a href="#">Course</a>
               <div class="dropdown-content">
                   <a href="ViewCourse.jsp">View Course</a>
                   <a href="Addnewcourse.jsp">Add New Course</a>
                   <a href="modifycourse.jsp">Modify Course</a>
                   <a href="deletecourse.jsp">Delete Course</a>
                </div>
               </li>
               
                <li class="dropdown"><a href="#">Students</a>
                <div class="dropdown-content">    
                    <a href="viewallstudents.jsp">View Students</a>
                    <a href="addstudent.jsp">Add Students</a>
                    <a href="modifystudent.jsp">Modify Students</a>
                    <a href="deletestudent.jsp">Delete Students</a>
                </div>
                </li>    
               
               
                <li class="dropdown"><a href="#">Enroll</a>
                <div class="dropdown-content">
                    <a href="viewallstudent.jsp">Remove</a>
                    <a href="addstudent.jsp">Enroll</a>
                 </div>
                </li>    
                <li class="dropdown"><a href="Fees.jsp">Fees</a></li>
                        
               <li style="float:right"><a href="logout.jsp">Logout</a></li>
            </ul>
        </div>
<%
      Connection conn = null;
      String url = "jdbc:mysql://localhost:3306/";
      String dbName = "Student table";
      String driver = "com.mysql.jdbc.Driver";
      String userName ="root";
      String password="";
      Statement st;
      try {
         Class.forName(driver).newInstance();
         conn = DriverManager.getConnection(url+dbName,userName,password);
         st=conn.createStatement();
           
         String id = request.getParameter("SelectedStudents");
          
         String query = "Select * from StudentMaster where Id="+id+" ";
         ResultSet rs=st.executeQuery(query);
    %>  
    <form method="post" action="modifystudent_screen.jsp">
            <table id="course-table" align ="center">
                   
      <%   while(rs.next()){
      %>
         
     
        <tr>
        <th>Id</th>    
        <td><input type="text" value="<%=rs.getInt("Id")%>" name="Id" readonly></td>
        </tr>
        <tr>
         <th>Name</th>   
        <td><input type="text" value="<%=rs.getString("name")%>" name="name"></td>
        </tr>
        <tr>
        <th>Password</th>    
        <td><input type="text" value="<%=rs.getString("password")%>" name="password"></td>
        </tr>
        <tr>
        <th>Email</th>    
        <td><input type="text" value="<%=rs.getString("email")%>" name="email"></td>
        </tr>
        <tr>
        <th>Mobile</th>    
        <td><input type="text" value="<%=rs.getInt("mobile")%>" name="mobile"></td>
        </tr>
        <tr>
        <th>City</th>    
        <td><input type="text" value="<%=rs.getString("city")%>" name="city"></td>
        </tr>
        
      <%}
      %> <tr>
         <td></td> 
         <td>
         <input type="submit" value="Modify"></td></tr>
        </table><br><br>
      </form>
        
      <%    } catch (Exception e) {
                out.println(e);
            }
      %>
    </body>
</html>
