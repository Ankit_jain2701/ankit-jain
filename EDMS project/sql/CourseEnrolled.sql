/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  USER
 * Created: 15 Jan, 2019
 */

Create Table CourseEnrolled(
Id Integer auto_increment,
StudentId integer,
CourseId integer,
PRIMARY KEY(Id),
FOREIGN KEY(StudentId)
REFERENCES StudentMaster(Id),
FOREIGN KEY(CourseId)
REFERENCES CourseMaster(Id)
);
Select * from CourseEnrolled;