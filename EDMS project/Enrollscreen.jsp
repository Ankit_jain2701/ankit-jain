<%-- 
    Document   : Enrollscreen
    Created on : 18 Jan, 2019, 8:22:12 PM
    Author     : USER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link rel="stylesheet" href="Home1page.css">
    </head>
    <body>
       <div id ="navHome">
            <ul>
               <li class="active"><a href="Home1page.jsp">Home</a></li>
               
               <li class="dropdown"><a href="#">Course</a>
               <div class="dropdown-content">
                   <a href="ViewCourse.jsp">View Course</a>
                   <a href="Addnewcourse.jsp">Add New Course</a>
                   <a href="modifycourse.jsp">Modify Course</a>
                   <a href="deletecourse.jsp">Delete Course</a>
                </div>
               </li>
               
                <li class="dropdown"><a href="#">Students</a>
                <div class="dropdown-content">    
                    <a href="viewallstudents.jsp">View Students</a>
                    <a href="addstudent.jsp">Add Students</a>
                    <a href="modifystudent.jsp">Modify Students</a>
                    <a href="deletestudent.jsp">Delete Students</a>
                </div>
                </li>    
               
               
                <li class="dropdown"><a href="#">Enroll</a>
                <div class="dropdown-content">
                    <a href="viewallstudent.jsp">Remove</a>
                    <a href="addstudent.jsp">Enroll</a>
                 </div>
                </li>    
                <li class="dropdown"><a href="Fees.jsp">Fees</a></li>
                        
               <li style="float:right"><a href="logout.jsp">Logout</a></li>
            </ul>
        </div>
        <table id="course-table" align ="center">
            
        </table>    
    </body>
</html>
